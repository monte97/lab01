package lab01.example.model;

/**
 * This class permits to extend the capability of a bank account by introducing an ATM
 */
public class ATMBankAccount implements BankAccountWithAtm {

    private final BankAccount baseBankAccount;
    private final FeeCalculator feeCalculator;

    public ATMBankAccount(BankAccount baseBankAccount, FeeCalculator feeCalculator) {
        this.baseBankAccount = baseBankAccount;
        this.feeCalculator = feeCalculator;
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        this.baseBankAccount.deposit(usrID, amount - feeCalculator.calculateFee(amount));
    }

    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        this.baseBankAccount.withdraw(usrID, amount + feeCalculator.calculateFee(amount));
    }

    @Override
    public AccountHolder getHolder() {
        return this.baseBankAccount.getHolder();
    }

    @Override
    public double getBalance() {
        return this.baseBankAccount.getBalance();
    }

    @Override
    public void deposit(int usrID, double amount) {
        this.baseBankAccount.deposit(usrID, amount);
    }

    @Override
    public void withdraw(int usrID, double amount) {
        this.baseBankAccount.withdraw(usrID, amount);
    }
}
