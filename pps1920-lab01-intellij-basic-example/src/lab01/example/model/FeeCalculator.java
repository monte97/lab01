package lab01.example.model;

public interface FeeCalculator {

    double calculateFee(double ammount);
}
